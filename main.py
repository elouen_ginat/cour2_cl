from fuzzywuzzy import fuzz

print("hello")

ratio = fuzz.ratio("bonjour","bonsoir")

print(ratio)

#===========
#API
#===========

import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def root():
    return {"result": "ok"}

@app.get("/hello")
def hello():

    ratio = fuzz.ratio("bonjour","bonsoir")

    return {"hello": ratio}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
